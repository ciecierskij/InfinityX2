#ifndef PROJECT_MATH_H
#define PROJECT_MATH_H

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

namespace ifx{
void math();
}

#endif //PROJECT_MATH_H
