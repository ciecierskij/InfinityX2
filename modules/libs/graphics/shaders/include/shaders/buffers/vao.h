#ifndef DUCK_VAO_H
#define DUCK_VAO_H

#include <GL/glew.h>
#include "shaders/buffers/vbo.h"
#include "shaders/buffers/ebo.h"

class VAO {
private:
    GLuint id;
public:

    VAO();

    ~VAO();

    void bindBuffers(VBO &vbo, EBO &ebo);

    void bind();
    void unbind();
};


#endif //DUCK_VAO_H
